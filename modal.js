(function () {
    if (typeof window.CustomEvent === "function") return false;
    function CustomEvent(event, params) {
        params = params || { bubbles: false, cancelable: false, detail: null };
        let evt = document.createEvent('CustomEvent');
        evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);
        return evt;
    }
    window.CustomEvent = CustomEvent;
})();

$modal = function (options) {
    let
        _elemModal,
        _eventShowModal,
        _eventHideModal,
        _hiding = false,
        _destroyed = false,
        _animationSpeed = 200;

    function _createModal(options) {
        let
            elemModal = document.createElement('div'),
            modalTemplate = '<div class="modal__backdrop" data-dismiss="modal"><div id="modal__content" class="modal__content"><div id="modal__header" class="modal__header"><span id="modal__btn-close" class="modal__btn-close" data-dismiss="modal" title="Закрыть">×</span></div><button id="show-modal-name" class="modal__body" data-modal="content">Имя</button><button id="show-modal-products" class="modal__body" data-modal="content">Управление товарами</button><button id="show-modal-balance" class="modal__body" data-modal="content">Баланс</button>{{footer}}</div></div>',
            modalFooterTemplate = '<div class="modal__footer">{{buttons}}</div>',
            modalButtonTemplate = '<button type="button" class="{{button_class}}" data-handler={{button_handler}}>{{button_text}}</button>',
            modalHTML,
            modalFooterHTML = '';

        elemModal.classList.add('modal');
        modalHTML = modalTemplate.replace('{{title}}', options.title || '');
        if (options.footerButtons) {
            for (let i = 0, length = options.footerButtons.length; i < length; i++) {
                let modalFooterButton = modalButtonTemplate.replace('{{button_class}}', options.footerButtons[i].class);
                modalFooterButton = modalFooterButton.replace('{{button_handler}}', options.footerButtons[i].handler);
                modalFooterButton = modalFooterButton.replace('{{button_text}}', options.footerButtons[i].text);
                modalFooterHTML += modalFooterButton;
            }
            modalFooterHTML = modalFooterTemplate.replace('{{buttons}}', modalFooterHTML);
        }
        modalHTML = modalHTML.replace('{{footer}}', modalFooterHTML);

        let deleteElements = function(){
            let del = document.querySelectorAll('.modal__body');
            del.forEach(function(button){
                button.parentNode.removeChild(button);
            });
        }

        let deleteNameElements = function(){
            let del = document.querySelectorAll('.justText');
            del.forEach(function(div){
                div.parentNode.removeChild(div);
            });
        }

        let replaceX = function(){
            document.getElementById("modal__btn-close").innerHTML = "<"
            document.getElementById("modal__btn-close").removeAttribute("data-dismiss");
        }
        
        window.onload = function(){
            document.getElementById('show-modal-name').onclick = function(){
                deleteElements();
                
                var myInnerDiv = document.createElement('div')
                var myTag = document.createElement("p");
                var myText = document.createTextNode("Имя");
                var myDiv = document.getElementById("modal__header");
                myTag.appendChild(myText);
                myInnerDiv.appendChild(myTag);
                myDiv.appendChild(myInnerDiv);
                myTag.classList.add('orangeText');
                myTag.classList.add('justText');

                var myInnerDiv = document.createElement('div')
                var myTag = document.createElement("p");
                var myText = document.createTextNode("Ваше текущее имя: ноунейм");
                var myDiv = document.getElementById("modal__header");
                myTag.appendChild(myText);
                myInnerDiv.appendChild(myTag);
                myDiv.appendChild(myInnerDiv);
                myTag.classList.add('whiteText');
                myTag.classList.add('justText');

                var myInnerDiv = document.createElement('div')
                var myTag = document.createElement("p");
                var myText = document.createTextNode("Изменить имя");
                var myDiv = document.getElementById("modal__header");
                myTag.appendChild(myText);
                myInnerDiv.appendChild(myTag);
                myDiv.appendChild(myInnerDiv);
                myTag.classList.add('whiteToOrange');
                myTag.classList.add('justText');

                replaceX();

                document.getElementById('modal__btn-close').onclick = function(){
                    deleteNameElements();
                };
            };
    
            document.getElementById('show-modal-products').onclick = function(){
                deleteElements();
    
                var myTag = document.createElement("p");
                var myText = document.createTextNode("Управление товарами");
                var myDiv = document.getElementById("modal__header");
                myTag.appendChild(myText);
                myDiv.appendChild(myTag);
                myTag.classList.add('orangeText');
                myTag.classList.add('justText');

                var myInnerDiv = document.createElement('div')
                var myTag = document.createElement("p");
                var myText = document.createTextNode("Добавить или удалить товар");
                var myDiv = document.getElementById("modal__header");
                myTag.appendChild(myText);
                myInnerDiv.appendChild(myTag);
                myDiv.appendChild(myInnerDiv);
                myTag.classList.add('whiteToOrange');
                myTag.classList.add('justText');

                var myInnerDiv = document.createElement('div')
                var myTag = document.createElement("p");
                var myText = document.createTextNode("Изменить пороговое значение для товаров");
                var myDiv = document.getElementById("modal__header");
                myTag.appendChild(myText);
                myInnerDiv.appendChild(myTag);
                myDiv.appendChild(myInnerDiv);
                myTag.classList.add('whiteToOrange');
                myTag.classList.add('justText');

                replaceX();

                document.getElementById('modal__btn-close').onclick = function(){
                    deleteNameElements();
                };
            };
    
            document.getElementById('show-modal-balance').onclick = function(){
                deleteElements();
    
                var myTag = document.createElement("p");
                var myText = document.createTextNode("Баланс");
                var myDiv = document.getElementById("modal__header");
                myTag.appendChild(myText);
                myDiv.appendChild(myTag);
                myTag.classList.add('orangeText');
                myTag.classList.add('justText');

                var myInnerDiv = document.createElement('div')
                var myTag = document.createElement("p");
                var myText = document.createTextNode("Ваш текущий баланс: 13256 руб.");
                var myDiv = document.getElementById("modal__header");
                myTag.appendChild(myText);
                myInnerDiv.appendChild(myTag);
                myDiv.appendChild(myInnerDiv);
                myTag.classList.add('whiteText');
                myTag.classList.add('justText');

                var myInnerDiv = document.createElement('div')
                var myTag = document.createElement("p");
                var myText = document.createTextNode("Изменить баланс");
                var myDiv = document.getElementById("modal__header");
                myTag.appendChild(myText);
                myInnerDiv.appendChild(myTag);
                myDiv.appendChild(myInnerDiv);
                myTag.classList.add('whiteToOrange');
                myTag.classList.add('justText');

                replaceX();

                document.getElementById('modal__btn-close').onclick = function(){
                    deleteNameElements();
                }
            };
        };

        elemModal.innerHTML = modalHTML;
        document.body.appendChild(elemModal);
        return elemModal;
    }

    function _showModal() {
        if (!_destroyed && !_hiding) {
            _elemModal.classList.add('modal__show');
            document.dispatchEvent(_eventShowModal);
        }
    }

    function _hideModal() {
        _hiding = true;
        _elemModal.classList.remove('modal__show');
        _elemModal.classList.add('modal__hiding');
        setTimeout(function () {
            _elemModal.classList.remove('modal__hiding');
            _hiding = false;
        }, _animationSpeed);
        document.dispatchEvent(_eventHideModal);
    }

    function _handlerCloseModal(e) {
        if (e.target.dataset.dismiss === 'modal') {
            _hideModal();
        }
    }

    _elemModal = _createModal(options || {});


    _elemModal.addEventListener('click', _handlerCloseModal);
    _eventShowModal = new CustomEvent('show.modal', { detail: _elemModal });
    _eventHideModal = new CustomEvent('hide.modal', { detail: _elemModal });

    return {
        show: _showModal,
        hide: _hideModal,
        destroy: function () {
            _elemModal.parentElement.removeChild(_elemModal),
                _elemModal.removeEventListener('click', _handlerCloseModal),
                _destroyed = true;
        },
        setContent: function (html) {
            _elemModal.querySelector('[data-modal="content"]').innerHTML = html;
        },
        setTitle: function (text) {
            _elemModal.querySelector('[data-modal="title"]').innerHTML = text;
        }
    }
};